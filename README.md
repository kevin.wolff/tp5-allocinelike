# TP5 - Allocine

**A rendre pour le 06/08/2020, avant 13:30**

[Lien vers la maquette](https://www.figma.com/file/TcMxqeytfOKQ4wXhyukn5t/Maquette-AlloCine-like?node-id=0%3A1)

### Consignes :

Créer un site internet de recherche d'informations sur des films. Grâce a ce site, on va pouvoir rechercher n'importe quel film et connaître toutes les infos liés au film (titre, durée, réalisateur, etc ..). Pour toutes les informations, nous allons utiliser l'API de [The Movie DB](https://developers.themoviedb.org/3/getting-started/introduction).

Le site doit proposer un champ de recherche qui permet de taper le titre d'un film et de faire une recherche sur la base. La recherche doit donner une liste des meilleures correspondance avec ce que l'utilisateur a tapé. Cette liste devra être sur plusieurs pages.

Chaque élément des resultats de recherche doit être cliquable et amène vers la page du film. Dans cette page, on affiche toutes les informations utiles pour le film. A vous de déterminer les informations les plus pertinentes.

Aucune maquette graphique n'a été définie pour le site, vous avez carte blanche.. Vous pouvez le créer en mono-page ou en multi-page. On va mettre l'accent sur l'UI/UX, l'accessibilité et le SEO du site.

Il s'agit d'un produit minimum viable. Vous pouvez ensuite ajouter toutes fonctionnalités qui vous semble pertinentes, exemples : rechercher des séries, rechercher des acteurs et réalisateurs et connaître leur filmographie, ajouter un carrousel des top films du moment, ajouter la possibilité de filtrer/trier les resultats de la recherche..

### Points obligatoires :

- réaliser en HTML5, CSS3, JS ES6+
- responsivité, respect des règles d'accessibilités et optimisé pour le SEO
- utiliser SASS pour la création de votre CSS
- utiliser un framework CSS de votre choix
- utiliser NPM pour la gestion de vos dépendances
- utiliser ESLint et StyleLint pour le style du code
- points facultatifs : vous pouvez utiliser n'importe quelle librairie JS ou CSS
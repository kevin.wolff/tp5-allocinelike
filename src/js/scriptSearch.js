// RECHERCHE - PARTIE REQUETE //

function searchPart() {
    // Variable input pour bar recherche
    let input = document.getElementById("recherche");

    // Variables valeurs de recherche
    let inputValue = document.getElementById("recherche").value;
    let selectboxMovie = document.getElementById("movie").checked;
    let selectboxTv = document.getElementById("tv").checked;

    // Si recherche film
    if (inputValue !== "" && selectboxMovie == true) {
        // Variables type et query pour local storage
        let section = inputValue; // Nom section
        let type = "movie"; // Type
        let query = `&query=${input.value}`; // Valeur requete
        let page = 1; // page
        // Fonction local storage + recherche.html
        loadSearch(section, type, query, page); // scriptSearch.js
    }
    // Si recherche tv
    else if (inputValue !== "" && selectboxTv == true) {
        // Variables type et query pour local storage
        let section = inputValue; // Nom section
        let type = "tv"; // Type
        let query = `&query=${input.value}`; // Valeur requete
        let page = 1; // page
        // Fonction local storage + recherche.html
        loadSearch(section, type, query, page); // scriptSearch.js
    }
    // Si formulaire invalide
    else {
        input.placeholder = "Renseigner un titre de film ..";
    };
}

// Local storage de la requete et ouvre la page recherche.html
function loadSearch(section, type, query, page) {
    localStorage.setItem("section", section) // Valeur section
    localStorage.setItem("type", type); // Valeur type
    localStorage.setItem("query", query); // Valeur query
    localStorage.setItem("page", page); // Valeur page
    window.location.href = "recherche.html";
}

// RECHERCHE - PARTIE RESULTATS //

function resultPart(section, datas, type, page) {
    let result = datas.results; // Recupere les datas.results

    // CONSTRUCTION RECHERCHE //

    // Creer div container
    let divContainer = document.createElement("div");
    divContainer.className = `divContainer ${type}`;
    // Creer nom section
    let sectionName = document.createElement("h2");
    sectionName.innerHTML = `Resultats pour : "${section}"`;
    // Creer container div content
    let containerContent = document.createElement("div");
    containerContent.className = "containerContentSearch";

    // Boucle pour afficher resultat de la recherche
    for (let i = 0; i < result.length; i++) {
        let element = result[i];

        // Creer div content
        let divContent = document.createElement("div");
        divContent.className = `divContent`;
        divContent.setAttribute("itemscope", ""); // SEO
        divContent.setAttribute("itemtype", "https://schema.org/thing"); // SEO
        // Creer img affiche film
        let poster = document.createElement("img");
        // Ecriture URL img
        let infoPath = element.poster_path; // Valeur poster_path
        if (infoPath === null) {
            let img = "src/img/afficheNull.jpg";
            poster.setAttribute("src", img);
            poster.setAttribute("alt", "pas d'affiche disponible");
        }
        else {
            let img = `https://image.tmdb.org/t/p/w154${infoPath}`;
            poster.setAttribute("src", img);
            let alt = element.name;
            poster.setAttribute("alt", `affiche du film ${alt}`);
            poster.setAttribute("itemprop", img); // SEO
        }
        // Creer titre
        let title = document.createElement("p");
        if (type === "movie") {
            title.innerHTML = element.title; // Valeur title
            title.setAttribute("itemprop", "name"); // SEO
        }
        else {
            title.innerHTML = element.name; // Valeur name
            title.setAttribute("itemprop", "name"); // SEO
        }
        // Creer note
        let rate = document.createElement("p");
        rate.innerHTML = `${element.vote_average * 10}%`; // Valeur vote_average
        // Creer progress bar
        let progress = document.createElement("div");
        progress.className = "progress";
        let progressValue = element.vote_average * 10; // Valeur width bar
        let progressBar = document.createElement("div");
        progressBar.className = "progressBar";
        progressBar.setAttribute("role", "progressbar");
        progressBar.setAttribute("aria-valuenow", "0");
        progressBar.setAttribute("aria-valuemin", "0");
        progressBar.setAttribute("aria-valuemax", "100");
        progressBar.style.width = `${progressValue}%`;
        // Recuperer ID
        let id = element.id; // Valeur id
        
        // Cible
        let cible = document.querySelector(".recherche");
        
        // AFFICHAGE RECHERCHE //

        // Div container
        cible.appendChild(divContainer);
        divContainer.appendChild(sectionName); // Section
        // Div container content
        divContainer.appendChild(containerContent);
        // Div content
        containerContent.appendChild(divContent);
        divContent.appendChild(poster); // Affiche
        divContent.appendChild(title); // Titre
        divContent.appendChild(rate); // Note
        divContent.appendChild(progress); // Div progress
        progress.appendChild(progressBar); // Progress bar

        // Fonction pour afficher detail du contenu
        // Event listener sur chaque divContent
        divContent.addEventListener("click", function() {
            loadDetails(id, type)
        })
    }

    // AFFICHER DETAIL D'UN FILM //

    // Local storage de l'id et ouvre la page contentCard.html
    function loadDetails(id, type) {
        localStorage.setItem("id", id);
        localStorage.setItem("type", type);
        window.location.href = "contentCard.html";
    }

    // PAGINATION //

    let pages = datas.total_pages; // Recupere le nombre de pages

    // Cible
    let pagination = document.querySelector(".pagination");

    if (pages > 1) {
        // Moins
        // Creer bouton
        let pageLess = document.createElement("li");
        pageLess.className = "btn btn-danger mx-2";
        pageLess.innerHTML = "<<";
        // Afficher bouton
        pagination.appendChild(pageLess);
        // Event listener
        pageLess.addEventListener("click", function() {
            if (page > 1) {
                page--; // Decremente
                localStorage.setItem("page", page); // Storage
                window.location.href = "recherche.html"; // Redirection
            }
        })

        // Page actuel
        // Creer paragraphe
        let pageActual = document.createElement("li");
        pageActual.className = "btn btn-danger mx-2";
        pageActual.innerHTML = `${page}`;
        // Afficher paragraphe
        pagination.appendChild(pageActual);

        // Plus
        // Creer bouton
        let pagePlus = document.createElement("li");
        pagePlus.className = "btn btn-danger mx-2";
        pagePlus.innerHTML = ">>";
        // Affiche bouton
        pagination.appendChild(pagePlus);
        // Event listener
        pagePlus.addEventListener("click", function() {
            if (page < pages) {
                page++; // Increment
                localStorage.setItem("page", page); // Storage
                window.location.href = "recherche.html"; // Redirection
            }        
        })
    }
    else {
        // Page actuel
        // Creer paragraphe
        let pageActual = document.createElement("li");
        pageActual.className = "btn btn-danger mx-2";
        pageActual.innerHTML = `${page}`;
        // Afficher paragraphe
        pagination.appendChild(pageActual);
    }
}

// EXPORT
export {searchPart, loadSearch, resultPart};
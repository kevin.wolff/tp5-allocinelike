// IMPORT //
import 'bootstrap';
import { searchPart, resultPart } from './scriptSearch';

// SCROOL TRANSFORM BANNER //

const banner = document.querySelector('.banner');
window.addEventListener('scroll', () => {
  if (window.scrollY > 150) {
    banner.classList.add('scroll');
  } else {
    banner.classList.remove('scroll');
  }
});

// RECHERCHE - PARTIE REQUETE //

// Variable bouton pour bar de recherche
const formButton = document.getElementById('formButton');
// Event listener sur le bouton
formButton.addEventListener('click', () => {
  searchPart(); // scriptSearch.js
});

// RECHERCHE - PARTIE TRAITEMENT DE LA REQUETE //

// Ecriture URL recherche
const section = localStorage.getItem('section'); // Valeur section
const type = localStorage.getItem('type'); // Valeur type
const query = localStorage.getItem('query'); // Valeur query
const page = localStorage.getItem('page'); // Valeur page

function search() {
  // Fonction pour creer la requete
  const searchUrl = `https://api.themoviedb.org/3/search/${type}?api_key=f346a4178fd4cc49acd6c9340a013152&language=fr${query}&page=${page}&include_adult=false`;

  // Requete pour la recherche
  fetch(`${searchUrl}`)
    .then((response) => response.json(), // Si la requete est valide
    )
    .then((datas) => {
      // Fonction pour creer et afficher le contenu de la requete
      resultPart(section, datas, type, page); // scriptSearch.js
    });
}

search(); // scriptRecherche.js

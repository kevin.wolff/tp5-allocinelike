// IMPORT //
import { displayContent } from './scriptDisplayContent';
import { searchPart } from './scriptSearch';

// SCROOL TRANSFORM BANNER //

const banner = document.querySelector('.banner');
window.addEventListener('scroll', () => {
  if (window.scrollY > 150) {
    banner.classList.add('scroll');
  } else {
    banner.classList.remove('scroll');
  }
});

// RECHERCHE - PARTIE REQUETE //

// Variable bouton pour bar de recherche
const formButton = document.getElementById('formButton');
// Event listener sur le bouton
formButton.addEventListener('click', () => {
  searchPart(); // scriptSearch.js
});

// AFFICHAGE DETAIL //

// Ecriture URL recherche
const type = localStorage.getItem('type');
const id = localStorage.getItem('id');

// REQUETE DETAIL D'UN FILM //

// Requete
fetch(`https://api.themoviedb.org/3/${type}/${id}?api_key=f346a4178fd4cc49acd6c9340a013152&language=fr`)
  .then((response) => response.json(), // Si la requete est valide
  )
  .then((datas) => {
    // CONSTRUCTION FICHE DETAIL D'UN FILM /l

    // Creer div container
    const divContainer = document.createElement('div');
    divContainer.className = 'divContainer';

    // Creer div presentation
    const divPres = document.createElement('div');
    divPres.className = 'pres';
    divPres.setAttribute('itemscope', ''); // SEO
    divPres.setAttribute('itemtype', 'https://schema.org/thing'); // SEO
    // Creer div presentation container
    const presContainer = document.createElement('div');
    presContainer.className = 'presContainer';
    // Creer div presentation container gauche
    const presContainerLeft = document.createElement('div');
    presContainerLeft.className = 'presContainerLeft';
    // Titre - gauche
    const title = document.createElement('h2');
    if (type === 'movie') {
      title.innerHTML = datas.title; // Valeur title
      title.setAttribute('itemprop', 'name'); // SEO
    } else {
      title.innerHTML = datas.name; // Valeur name
      title.setAttribute('itemprop', 'name'); // SEO
    }
    // Duree - gauche
    const runTime = document.createElement('p');
    if (type === 'movie') {
      runTime.innerHTML = `${datas.runtime}mn`; // Valeur runtime
    } else {
      runTime.innerHTML = `${datas.episode_run_time}mn`; // Valeur episode_run_time
    }
    // Creer div presentation container droit
    const presContainerRight = document.createElement('div');
    presContainerRight.className = 'presContainerRight';
    presContainerRight.setAttribute('itemscope', ''); // SEO
    presContainerRight.setAttribute('itemtype', 'https://schema.org/creativeWork'); // SEO
    // Creer note
    const rate = document.createElement('p');
    rate.innerHTML = `${datas.vote_average * 10}%`; // Valeur vote_average
    rate.setAttribute('itemprop', 'contentRating'); // SEO
    // Creer progress bar
    const progress = document.createElement('div');
    progress.className = 'progress';
    const progressValue = datas.vote_average * 10; // Valeur width bar
    const progressBar = document.createElement('div');
    progressBar.className = 'progressBar';
    progressBar.setAttribute('role', 'progressbar');
    progressBar.setAttribute('aria-valuenow', '0');
    progressBar.setAttribute('aria-valuemin', '0');
    progressBar.setAttribute('aria-valuemax', '100');
    progressBar.style.width = `${progressValue}%`;
    // Genre
    const genre = document.createElement('p');
    genre.innerText = datas.genres[0].name; // Valeur genre

    // Creer div detail
    const detailContent = document.createElement('div');
    detailContent.className = 'detailContent';
    // Creer img affiche film
    const poster = document.createElement('img');
    // Ecriture URL img
    const infoPath = datas.poster_path; // Valeur poster_path
    if (infoPath === null) {
      const img = 'src/img/afficheNull.jpg';
      poster.setAttribute('src', img);
    } else {
      const img = `https://image.tmdb.org/t/p/w342${infoPath}`;
      poster.setAttribute('src', img);
    }
    // Creer div a propos
    const divPropos = document.createElement('div');
    divPropos.className = 'detailPropos';
    divPropos.setAttribute('itemscope', ''); // SEO
    divPropos.setAttribute('itemtype', 'https://schema.org/thing'); // SEO
    // A propos
    const aPropos = document.createElement('p');
    aPropos.innerHTML = 'Synopsis';
    // A propos content
    const aProposContent = document.createElement('p');
    aProposContent.innerHTML = datas.overview; // Valeur overview
    aProposContent.setAttribute('itemprop', 'about'); // SEO
    // Creer div container infos
    const divInfos = document.createElement('div');
    divInfos.className = 'detailContainerInfo';
    // Creer div infos - date
    const divDate = document.createElement('div');
    divDate.className = 'detailInfo';
    divDate.setAttribute('itemscope', ''); // SEO
    divDate.setAttribute('itemtype', 'https://schema.org/creativeWork'); // SEO
    // Sortie
    const sortie = document.createElement('p');
    sortie.innerHTML = 'sortie :';
    // Sortie date
    const sortieDate = document.createElement('p');
    sortieDate.innerText = datas.release_date; // Valeur release_date
    sortieDate.setAttribute('itemprop', 'datePublished'); // SEO
    // Creer div infos - directeur
    const divDirecteur = document.createElement('div');
    divDirecteur.className = 'detailInfo';
    divDirecteur.setAttribute('itemscope', ''); // SEO
    divDirecteur.setAttribute('itemtype', 'https://schema.org/movie'); // SEO
    // Directeur
    const directeur = document.createElement('p');
    directeur.innerHTML = 'directeurs :';
    // Creer div infos - acteurs
    const divActeur = document.createElement('div');
    divActeur.className = 'detailInfo';
    divActeur.setAttribute('itemscope', ''); // SEO
    divActeur.setAttribute('itemtype', 'https://schema.org/movie'); // SEO
    // Acteur
    const acteur = document.createElement('p');
    acteur.innerText = 'acteurs :';
    // Cible
    const cible = document.querySelector('.details');
    // Creer background cible
    const bgPath = datas.backdrop_path; // Valeur background_path
    // Ecriture URL img
    if (bgPath === null) { } 
    else {
      const bg = `https://image.tmdb.org/t/p/original${bgPath}`;
      cible.style.background = `center / cover url(${bg})`;
    }

    // AFFICHAGE FICHE DETAIL D'UN FILM //

    // Div container
    cible.appendChild(divContainer);
    // Div presentation
    divContainer.appendChild(divPres);
    // Div presentation container
    divPres.appendChild(presContainer);
    // Div presentation left
    presContainer.appendChild(presContainerLeft);
    presContainerLeft.appendChild(title); // Titre
    presContainerLeft.appendChild(runTime); // Duree
    // Div presentation right
    presContainer.appendChild(presContainerRight);
    presContainerRight.appendChild(progress); // Div progress
    progress.appendChild(progressBar); // Progress bar
    presContainerRight.appendChild(rate); // Note
    // Affichage genre
    divPres.appendChild(genre);

    // Div Detail
    divContainer.appendChild(detailContent);
    // IMG
    detailContent.appendChild(poster); // Poster
    // Div a propos
    detailContent.appendChild(divPropos);
    divPropos.appendChild(aPropos); // "a propos :"
    divPropos.appendChild(aProposContent); // A propos content
    // Div infos
    divPropos.appendChild(divInfos);
    // Div info date
    divInfos.appendChild(divDate);
    divDate.appendChild(sortie); // "sortie :"
    divDate.appendChild(sortieDate); // Date
    // Div infos directeur
    divInfos.appendChild(divDirecteur);
    divDirecteur.appendChild(directeur); // "directeur :"
    // Div infos acteur
    divInfos.appendChild(divActeur);
    divActeur.appendChild(acteur); // "acteur :"

    // REQUETE CASTING D'UN FILM //

    // Requete
    fetch(`https://api.themoviedb.org/3/${type}/${id}/credits?api_key=f346a4178fd4cc49acd6c9340a013152`)
      .then((response) => response.json()), // Si la requete est valide
      .then((datas) => {
        // CONSTRUCTION FICHE CASTING D'UN FILM //

        const crews = datas.crew;
        const casts = datas.cast;

        // Boucle pour afficher les 4 premiers resultats
        for (let i = 0; i < 4 && i <= crews.length && i <= casts.length; i++) {
          // Creer paragraphe nom et post des membres du crew
          const crewName = document.createElement('h6');
          const crewPost = document.createElement('p');
          if (crews.length == 0) { }
          else {
            crewName.innerText = crews[i].name;
            crewName.setAttribute('itemprop', 'director'); // SEO
            crewPost.innerText = crews[i].department;
          }

          // Creer div cast
          const divCast = document.createElement('div');
          divCast.className = 'cast';
          // Creer img affiche film
          const profile = document.createElement('img');
          // Creer div info cast
          const divInfoCast = document.createElement('div');
          // Nom des membres du cast
          const castName = document.createElement('h6');
          castName.setAttribute('itemprop', 'actor'); // SEO
          // Role des membres du cast
          const roleName = document.createElement('p');
          if (casts.length == 0) {
          } else {
            // Ecriture URL img
            const profilePath = casts[i].profile_path; // Valeur profile_path
            if (profilePath === null) {
              const profileUrl = 'src/img/profileNull.jpg';
              profile.setAttribute('src', profileUrl);
            } else {
              const profileUrl = `https://image.tmdb.org/t/p/w45${profilePath}`;
              profile.setAttribute('src', profileUrl);
              castName.innerText = casts[i].name;
              roleName.innerText = casts[i].character;
            }
          }

          divDirecteur.appendChild(crewName); // Noms crew
          divDirecteur.appendChild(crewPost); // Post crew
          // Div cast
          divActeur.appendChild(divCast);
          // IMG
          divCast.appendChild(profile);
          // Div info cast
          divCast.appendChild(divInfoCast);
          divInfoCast.appendChild(castName); // Noms casting
          castName.appendChild(roleName); // Noms role
        }
      });
  });

// AFFICHAGE RECO //

// Requete
fetch(`https://api.themoviedb.org/3/${type}/${id}/similar?api_key=f346a4178fd4cc49acd6c9340a013152&language=fr&page=1`)
  .then((response) => response.json(), // Si la requete est valide
  )
  .then((datas) => {
    if (datas.results == 0) {
      // CONSTRUCTION SI 0 RECO //

      // Creer div container
      const divContainer = document.createElement('div');
      divContainer.className = `divContainer ${type}`;
      // Creer nom section
      const sectionName = document.createElement('h2');
      sectionName.innerHTML = 'Pas de recommendations';

      // Cible
      const cible = document.querySelector(`.${type}`);

      // AFFICHAGE SI 0 RECO //

      // Affiche div container
      cible.appendChild(divContainer);
      divContainer.appendChild(sectionName); // Section
    }

    // SI RECO FONCTION POUR CREER ET AFFICHER CONTENU DE LA REQUETE
    else {
      displayContent(datas, type, 'Recommendations'); // scriptDisplayContent
    }
  });

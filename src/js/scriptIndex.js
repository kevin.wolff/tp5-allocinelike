// IMPORT //
import 'bootstrap';
import '../sass/style.scss';
import { displayContent } from './scriptDisplayContent';
import { searchPart } from './scriptSearch';

// SCROOL TRANSFORM BANNER //

const banner = document.querySelector('.banner');
window.addEventListener('scroll', () => {
  if (window.scrollY > 150) {
    banner.classList.add('scroll');
  } else {
    banner.classList.remove('scroll');
  }
});

// RECHERCHE - PARTIE REQUETE //

// Variable bouton pour bar de recherche
const formButton = document.getElementById('formButton');
// Event listener sur le bouton
formButton.addEventListener('click', () => {
  searchPart(); // scriptSearch.js
});

// BOUTONS SELECTEUR FILM / TV //

// Variables boutons film / tv
const movieButton = document.getElementById('movieButton');
const tvButton = document.getElementById('tvButton');
// Variable div film / tv
const movie = document.querySelector('.movie');
const tv = document.querySelector('.tv');
tv.style.display = 'none';
// Event listerner bouton film
movieButton.addEventListener('click', () => {
  movie.style.display = 'block';
  movieButton.style.borderBottom = '3px solid #ff6348';
  tv.style.display = 'none';
  tvButton.style.borderBottom = 'none';
});
// Event listerner bouton tv
tvButton.addEventListener('click', () => {
  movie.style.display = 'none';
  movieButton.style.borderBottom = 'none';
  tv.style.display = 'block';
  tvButton.style.borderBottom = '3px solid #ff6348';
});

// AFFICHAGE FILM //

// POPULAIRE
// Requete
fetch('https://api.themoviedb.org/3/movie/popular?api_key=f346a4178fd4cc49acd6c9340a013152&language=fr')
  .then((response) => response.json(), // Si la requete est valide
  )
  .then((datas) => {
    // Fonction pour creer et afficher le contenu de la requete
    displayContent(datas, 'movie', 'Films populaires'); // scriptDisplayContent.js
  });

// TRENDING
// Requete
fetch('https://api.themoviedb.org/3/trending/movie/day?api_key=f346a4178fd4cc49acd6c9340a013152')
  .then((response) => response.json(), // Si la requete est valide
  )
  .then((datas) => {
    // Fonction pour creer et afficher le contenu de la requete
    displayContent(datas, 'movie', 'Films recherchés'); // scriptDisplayContent.js
  });

// TOP RATED
// Requete
fetch('https://api.themoviedb.org/3/movie/top_rated?api_key=f346a4178fd4cc49acd6c9340a013152&language=fr')
  .then((response) => response.json(), // Si la requete est valide
  )
  .then((datas) => {
    // Fonction pour creer et afficher le contenu de la requete
    displayContent(datas, 'movie', 'Films bien notés'); // scriptDisplayContent.js
  });

// AFFICHAGE TV //

// POPULAIRE
// Requete
fetch('https://api.themoviedb.org/3/tv/popular?api_key=f346a4178fd4cc49acd6c9340a013152&language=fr')
  .then((response) => response.json(), // Si la requete est valide
  )
  .then((datas) => {
    // Fonction pour creer et afficher le contenu de la requete
    displayContent(datas, 'tv', 'Séries populaires'); // scriptDisplayContent.js
  });

// TRENDING
// Requete
fetch('https://api.themoviedb.org/3/trending/tv/day?api_key=f346a4178fd4cc49acd6c9340a013152')
  .then((response) => response.json(), // Si la requete est valide
  )
  .then((datas) => {
    // Fonction pour creer et afficher le contenu de la requete
    displayContent(datas, 'tv', 'Séries recherchés'); // scriptDisplayContent.js
  });

// TOP RATED
// Requete
fetch('https://api.themoviedb.org/3/tv/top_rated?api_key=f346a4178fd4cc49acd6c9340a013152&language=fr')
  .then((response) => response.json(), // Si la requete est valide
  )
  .then((datas) => {
    // Fonction pour creer et afficher le contenu de la requete
    displayContent(datas, 'tv', 'Séries bien notés'); // scriptDisplayContent.js
  });

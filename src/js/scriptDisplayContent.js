// AFFICHAGE X ELEMENTS //

function displayContent(datas, type, section) {
    let result = datas.results; // Recupere les datas.results

    // CONSTRUCTION 6 ELEMENTS //

    // Creer div container
    let divContainer = document.createElement("div");
    divContainer.className = `divContainer ${type}`;
    // Creer nom section
    let sectionName = document.createElement("h2");
    sectionName.innerHTML = section;
    // Creer container div content
    let containerContent = document.createElement("div");
    containerContent.className = "containerContent";

    // Boucle pour afficher X ELEMENTS
    for (let i = 0; i < 10; i++) {
        let element = result[i];        

        // Creer div content
        let divContent = document.createElement("div");
        divContent.className = "divContent";
        divContent.setAttribute("itemscope", ""); // SEO
        divContent.setAttribute("itemtype", "https://schema.org/thing"); // SEO
        // Creer img affiche film
        let poster = document.createElement("img");
        // Ecriture URL img
        let infoPath = element.poster_path; // Valeur poster_path
        if (infoPath === null) {
            let img = "src/img/afficheNull.jpg";
            poster.setAttribute("src", img);
            poster.setAttribute("alt", "pas d'affiche disponible");
        }
        else {
            let img = `https://image.tmdb.org/t/p/w154${infoPath}`;
            poster.setAttribute("src", img);
            let alt = element.name;
            poster.setAttribute("alt", `affiche du film ${alt}`);
            poster.setAttribute("itemprop", img); // SEO
        }
        // Creer titre
        let title = document.createElement("p");
        if (type === "movie") {
            title.innerHTML = element.title; // Valeur title
            title.setAttribute("itemprop", "name"); // SEO
        }
        else {
            title.innerHTML = element.name; // Valeur name
            title.setAttribute("itemprop", "name"); // SEO
        }
        // Creer note
        let rate = document.createElement("p");
        rate.innerHTML = `${element.vote_average * 10}%`; // Valeur vote_average
        // Creer progress bar
        let progress = document.createElement("div");
        progress.className = "progress";
        let progressValue = element.vote_average * 10; // Valeur width bar
        let progressBar = document.createElement("div");
        progressBar.className = "progressBar";
        progressBar.setAttribute("role", "progressbar");
        progressBar.setAttribute("aria-valuenow", "0");
        progressBar.setAttribute("aria-valuemin", "0");
        progressBar.setAttribute("aria-valuemax", "100");
        progressBar.style.width = `${progressValue}%`;
        // Recuperer ID
        let id = element.id; // Valeur id
        
        // Cible 
        let cible = document.querySelector(`.${type}`);
        
        // AFFICHAGE DES 6 ELEMENTS //

        // Div container
        cible.appendChild(divContainer);
        divContainer.appendChild(sectionName); // Section
        // Div containerContent
        divContainer.appendChild(containerContent);
        // Div content
        containerContent.appendChild(divContent);
        divContent.appendChild(poster); // Affiche
        divContent.appendChild(title); // Titre
        divContent.appendChild(rate); // Note
        divContent.appendChild(progress); // Div progress
        progress.appendChild(progressBar); // Progress bar

        // Fonction pour afficher detail du contenu
        // Event listener sur chaque divContent
        divContent.addEventListener("click", function() {
            loadDetails(id, type)
        })
    }
}

// AFFICHER DETAIL D'UN FILM //

// Local storage de l'id et ouvre la page contentCard.html
function loadDetails(id, type) {
    localStorage.setItem("id", id);
    localStorage.setItem("type", type);
    window.location.href = "contentCard.html";
}

// EXPORT
export {displayContent};